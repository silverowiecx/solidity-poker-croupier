const {expect} = require("chai");
const {ethers} = require("hardhat");

const {CroupierSigner} = require('../scripts/CroupierSigner');


const ETHinWEI = "1000000000000000000";


describe("Croupier", function () {

    let owner, alice, bob, charlie;
    let aliceSigner, bobSigner, charlieSigner;
    let chips;
    let croupier;
    let gameParticipants;
    let gameBalances;


    before(async () => {
        [owner, alice, bob, charlie] = await ethers.getSigners();

        const Chips = await ethers.getContractFactory("PokerChips");
        chips = await Chips.deploy();

        const Croupier = await ethers.getContractFactory("Croupier");
        croupier = await Croupier.deploy(chips.address);

        chips.connect(alice).mintChips(1000, {value: ETHinWEI});
        chips.connect(bob).mintChips(1000, {value: ETHinWEI});
        chips.connect(charlie).mintChips(1000, {value: ETHinWEI});

        aliceSigner = new CroupierSigner(croupier, alice);
        bobSigner = new CroupierSigner(croupier, bob);
        charlieSigner = new CroupierSigner(croupier, charlie);

        gameParticipants = [alice.address, bob.address, charlie.address];
    });


    it("Creates new game", async function () {
        chips.connect(alice).approve(croupier.address, 100);
        chips.connect(bob).approve(croupier.address, 100);
        chips.connect(charlie).approve(croupier.address, 100);

        await expect(croupier.connect(alice).newGame(gameParticipants, 100)).to.emit(croupier, "NewGame");
    });

    it("Rejects updating the game for wrong balances", async function () {
        gameBalances = [300, 0, 250];
        let signatures = [];


        signatures.push(await aliceSigner.createSignature(1, gameBalances, true));
        signatures.push(await bobSigner.createSignature(1, gameBalances, true));
        signatures.push(await charlieSigner.createSignature(1, gameBalances, true));


        let gameUpdateObj =
            {
                "gameId": "1",
                "balances": gameBalances,
                "active": true
            };

        await expect(croupier.connect(alice).updateGame(gameUpdateObj, signatures)).to.be.revertedWith("Something's wrong with balances");
    });

    it("Rejects updating the game for too few signatures", async function () {
        gameBalances = [50, 50, 200];
        let signatures = [];


        signatures.push(await aliceSigner.createSignature(1, gameBalances, true));


        let gameUpdateObj =
            {
                "gameId": "1",
                "balances": gameBalances,
                "active": true
            };

        await expect(croupier.connect(alice).updateGame(gameUpdateObj, signatures)).to.be.revertedWith("Amount of signers is not enough");
    });

    it("Rejects updating the game for faked signature", async function () {
        gameBalances = [50, 50, 200];
        let signatures = [];


        signatures.push(await aliceSigner.createSignature(1, gameBalances, true));
        signatures.push(await bobSigner.createSignature(1, gameBalances, true));
        signatures.push(await (new CroupierSigner(croupier, owner)).createSignature(1, gameBalances, true));


        let gameUpdateObj =
            {
                "gameId": "1",
                "balances": gameBalances,
                "active": true
            };

        await expect(croupier.connect(alice).updateGame(gameUpdateObj, signatures)).to.be.revertedWith("Player not found or signature is invalid.");
    });

    it("Rejects updating the game for duplicated signature", async function () {
        gameBalances = [50, 50, 200];
        let signatures = [];


        signatures.push(await aliceSigner.createSignature(1, gameBalances, true));
        signatures.push(await aliceSigner.createSignature(1, gameBalances, true));


        let gameUpdateObj =
            {
                "gameId": "1",
                "balances": gameBalances,
                "active": true
            };

        await expect(croupier.connect(alice).updateGame(gameUpdateObj, signatures)).to.be.revertedWith("Duplicated signature");
    });

    it("Updates the game", async function () {
        gameBalances = [50, 50, 200];
        let signatures = [];


        signatures.push(await aliceSigner.createSignature(1, gameBalances, true));
        signatures.push(await bobSigner.createSignature(1, gameBalances, true));
        signatures.push(await charlieSigner.createSignature(1, gameBalances, true));


        let gameUpdateObj =
            {
                "gameId": "1",
                "balances": gameBalances,
                "active": true
            };

        await expect(croupier.connect(alice).updateGame(gameUpdateObj, signatures)).to.emit(croupier, "GameUpdated").withArgs(1);
    });


    it("Accepts deposits", async function () {
        await chips.connect(charlie).approve(croupier.address, 50);
        await expect(croupier.connect(charlie).increaseDeposit(1, 50)).to.emit(croupier, "NewDeposit").withArgs(1, charlie.address, 50);
    });

    it("Ends the game", async function () {
        gameBalances = [0, 0, 350];
        let signatures = [];

        signatures.push(await aliceSigner.createSignature(1, gameBalances, true));
        signatures.push(await bobSigner.createSignature(1, gameBalances, true));
        signatures.push(await charlieSigner.createSignature(1, gameBalances, true));

        let gameUpdateObj =
            {
                "gameId": "1",
                "balances": gameBalances,
                "active": true
            };
        await expect(croupier.connect(charlie).endGame(gameUpdateObj, signatures)).to.emit(croupier, "GameUpdated").withArgs(1);
    });

});
