const {expect} = require("chai");
const {ethers} = require("hardhat");
const {BigNumber} = require("ethers");

const ETHinWEI = "1000000000000000000";

describe("Chips", function () {

    let owner, alice, bob;
    let chips;


    before(async () => {
        [owner, alice, bob] = await ethers.getSigners();

        const Chips = await ethers.getContractFactory("PokerChips");
        chips = await Chips.connect(owner).deploy();
    });


    it("Mints new chips for ether payment", async function () {
        const balanceBefore = BigNumber.from(await alice.getBalance());

        // 1000 Chips for 1 ETH = 1000000000000000000
        chips.connect(alice).mintChips(1000, {value: ETHinWEI});

        expect(await chips.balanceOf(alice.address)).to.be.equal(1000);
        expect(await ethers.provider.getBalance(chips.address)).to.be.equal(ETHinWEI);
    });

    it("Doesnt allow owner to mint wihtout payment", async function () {
        await expect(chips.connect(owner).mintChips(1000)).to.be.reverted;
    });


    it("Transfers", async function () {
        await chips.connect(alice).transfer(bob.address, 1000);
        expect(await chips.balanceOf(alice.address)).to.be.equal(0);
        expect(await chips.balanceOf(bob.address)).to.be.equal(1000);
    });

    it("Burns for ETH", async function () {
        const balanceBefore = BigNumber.from(await ethers.provider.getBalance(bob.address));

        await chips.connect(bob).burnChips(1000);

        expect(await chips.balanceOf(bob.address)).to.be.equal(0);
        expect(await ethers.provider.getBalance(chips.address)).to.equal(0);
    });

    it("returns decimals", async function() {
       expect(await chips.decimals()).to.equal(0);
    });

});
