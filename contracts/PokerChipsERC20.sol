// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

/// @custom:security-contact silverowiec@gmail.com
contract PokerChips is ERC20 {

    uint256 public PRICE = 0.001 ether;

    constructor() ERC20("Poker Chips", "CHIP") {}


    function mintChips(uint256 amount) public payable {
        require(amount * PRICE <= msg.value, "Received insufficient amount of funds");
        _mint(msg.sender, amount);
    }

    function burnChips(uint256 amount) public {
        require(balanceOf(msg.sender) >= amount, "Insufficient amount of tokens");
        uint256 payback = amount * PRICE;
        _burn(msg.sender, amount);
        payable(msg.sender).transfer(payback);
    }


    function decimals() public view virtual override returns (uint8){
        return 0;
        // 10^0, single chips, no decimals
    }


}