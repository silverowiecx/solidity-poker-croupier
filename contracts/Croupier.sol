// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import './PokerChipsERC20.sol';

import "@openzeppelin/contracts/utils/Counters.sol";

import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import "@openzeppelin/contracts/utils/cryptography/draft-EIP712.sol";

import 'hardhat/console.sol';

contract Croupier is EIP712 {
    string private constant SIGNING_DOMAIN = "CroupierSGH";
    string private constant SIGNATURE_VERSION = "1";

    using Counters for Counters.Counter;
    Counters.Counter internal _gameIds;

    PokerChips chips;

    mapping(uint256 => Game) games;

    uint256 MAX_NO_PLAYERS = 8;

    struct Game {
        uint256 deposit; // for verification
        address[] players;
        uint256[] balances;
        bool active;
    }

    struct GameUpdate {
        uint256 gameId;
        uint256[] balances;
        bool active;
    }

    event NewGame(Game);
    event GameUpdated(uint256);
    event NewDeposit(uint256, address, uint256);

    constructor(address chips_)
    EIP712(SIGNING_DOMAIN, SIGNATURE_VERSION)
    {
        chips = PokerChips(chips_);
    }

    function newGame(address[] memory players, uint256 deposit) public {
        require(players.length < 9, "Croupier: Too many players.");
        require(deposit >= players.length * 2, "Croupier: Too small deposit.");

        for (uint8 i = 0; i < players.length; i++) {
            require(chips.allowance(players[i], address(this)) >= deposit, "Croupier: Allowance for chips is too small.");
        }
        _gameIds.increment();
        uint256 currentId = _gameIds._value;

        games[currentId].players = players;
        games[currentId].deposit = deposit * players.length;
        games[currentId].balances = new uint256[](players.length);

        for (uint8 i = 0; i < players.length; i++) {
            chips.transferFrom(players[i], address(this), deposit);
            games[currentId].balances[i] = deposit;
        }

        games[currentId].active = true;

        emit NewGame(games[currentId]);
    }


    function increaseDeposit(uint256 gameId, uint256 amount) public {
        require(chips.allowance(msg.sender, address(this)) >= amount, "Croupier: Allowance for chips is too small.");
        require(games[gameId].active, "Game already ended");
        chips.transferFrom(msg.sender, address(this), amount);
        games[gameId].balances[getPlayerId(gameId, msg.sender)] += amount;
        games[gameId].deposit += amount;

        emit NewDeposit(gameId, msg.sender, amount);
    }

    function updateGame(GameUpdate calldata update, bytes[] memory signatures) public payable {
        require(_gameIds._value >= update.gameId, "Game does not exist!");
        require(games[update.gameId].active, "Game already ended");
        require(_verifySignatures(update, signatures) > games[update.gameId].players.length / 2, "Amount of signers is not enough");

        _updateGame(update.gameId, update.balances);
    }

    function endGame(GameUpdate calldata update, bytes[] memory signatures) public payable {
        require(_gameIds._value >= update.gameId, "Game does not exist!");
        require(games[update.gameId].active, "Game already ended");

        require(_verifySignatures(update, signatures) > games[update.gameId].players.length / 2, "Amount of signers is not enough");


        _updateGame(update.gameId, update.balances);
        _endGame(update.gameId);
    }


    function _verifySignatures(GameUpdate calldata update, bytes[] memory signatures) private view returns (uint8){
        uint8 numberOfVerified = 0;
        address[] memory verified = new address[](signatures.length);
        address check;

        for (uint8 i = 0; i < signatures.length; i++) {
            check = _recoverAddress(update, signatures[i]);
            // checks if address points to player of current game
            getPlayerId(update.gameId, check);
            require(_containsNot(check, verified), "Duplicated signature");

            verified[numberOfVerified] = check;
            numberOfVerified += 1;
        }
        return numberOfVerified;
    }


    function _updateGame(uint256 gameId, uint256[] memory balances) private {
        uint256 total = 0;
        for (uint8 i = 0; i < balances.length; i++) {
            total += balances[i];
        }
        require(total == games[gameId].deposit, "Something's wrong with balances");
        games[gameId].balances = balances;
        emit GameUpdated(gameId);
    }

    function _endGame(uint256 gameId) private {
        Game memory currentGame = games[gameId];
        uint256 currentBalance;
        for (uint8 i = 0; i < currentGame.players.length; i++) {
            currentBalance = currentGame.balances[i];
            if (currentBalance > 0) {
                chips.transfer(currentGame.players[i], currentGame.balances[i]);
                currentGame.balances[i] = 0;
            }
        }
        games[gameId].active = false;

        emit GameUpdated(gameId);
    }


    function getPlayerId(uint256 gameId, address playerAddress) public view returns (uint8){
        for (uint8 i = 0; i < games[gameId].players.length; i++) {
            if (games[gameId].players[i] == playerAddress) {
                return i;
            }
        }
        revert("Player not found or signature is invalid.");
    }

    function _containsNot(address address_, address[] memory collection) private pure returns (bool){
        for (uint8 i = 0; i < collection.length; i++) {
            if (address_ == collection[i]) {
                return false;
            }
        }
        return true;
    }

    function _hash(GameUpdate calldata update) private view returns (bytes32) {
        return _hashTypedDataV4(
            keccak256(
                abi.encode(
                    keccak256("GameUpdate(uint256 gameId,uint256[] balances,bool active)"),
                    update.gameId,
                    keccak256(abi.encodePacked(update.balances)),
                    update.active
                )
            )
        );
    }


    function _recoverAddress(GameUpdate calldata update, bytes memory signature) private view returns (address){
        bytes32 digest = _hash(update);
        return ECDSA.recover(digest, signature);
    }

    function getChainID() external view returns (uint256) {
        uint256 id;
        assembly {
            id := chainid()
        }
        return id;
    }

}
