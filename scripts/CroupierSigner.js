const ethers = require('ethers');

// These constants must match the ones used in the smart contract.
const SIGNING_DOMAIN_NAME = 'CroupierSGH';
const SIGNING_DOMAIN_VERSION = '1';


class CroupierSigner {


    constructor(contract, signer) {
        this.contract = contract;
        this.signer = signer;
    }

    async createSignature(gameId, balances, active) {
        const gameUpdate = {gameId, balances, active};
        const domain = await this._signingDomain();
        const types = {
            GameUpdate: [
                {name: 'gameId', type: 'uint256'},
                {name: 'balances', type: 'uint256[]'},
                {name: 'active', type: 'bool'},
            ],
        };

        return await this.signer._signTypedData(domain, types, gameUpdate);
    }

    /**
     * @private
     * @returns {object} the EIP-721 signing domain, tied to the chainId of the signer
     */
    async _signingDomain() {
        if (this._domain != null) {
            return this._domain;
        }
        const chainId = parseInt(await this.contract.getChainID());
        this._domain = {
            name: SIGNING_DOMAIN_NAME,
            version: SIGNING_DOMAIN_VERSION,
            verifyingContract: this.contract.address,
            chainId,
        };
        return this._domain;
    }
}

module.exports = {
    CroupierSigner: CroupierSigner,
};
